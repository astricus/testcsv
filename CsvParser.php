<?php

Class CsvParser {

    /**
     * @param $csv_file
     * @return array
     */
    public function parse($csv_file){
        $file_data = explode(PHP_EOL, $csv_file );
        $parsed_data = array();
        foreach ($file_data as $value){
            $data = explode(";", $value );
            $parsed = array(
                'count' => $data[2],
                'articul' => $data[0],
                'price' => $data[1],
            );
            $parsed_data[] = $parsed;
        }
        return $parsed_data;
    }

}