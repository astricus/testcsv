<?php

Class Model {

    private $link;

    /**
     * Model constructor.
     * @param $host
     * @param $user
     * @param $password
     * @param $db
     */
    public function __construct($host, $user, $password, $db){
        mb_internal_encoding( 'UTF-8' );
        mb_regex_encoding( 'UTF-8' );
        mysqli_report( MYSQLI_REPORT_STRICT );
        try {
            $this->link = mysqli_connect( $host, $user, $password, $db );
            $this->link->set_charset( "utf8" );
        } catch ( Exception $e ) {
            die( 'Unable to connect to database' );
        }
    }

    /**
     * @param $product
     * @return mixed
     */
    private function get_product_articul($product){
        return $product['articul'];
    }

    /**
     * @param $product
     * @return int
     */
    public function check_product($product){
        $articul = $this->get_product_articul($product);
        $query = "SELECT count(*) as counter FROM test WHERE ARTICUL='" . $articul . "'";
        $check_product = mysqli_query($this->link, $query);
        while ($row = mysqli_fetch_row($check_product)) {
            $check_product_count = $row[0];
        }
        if($check_product_count) {
            return $check_product_count;
        } else {
            return 0;
        }
    }

    /**
     * @param $product
     * @return bool|mysqli_result
     */
    public function update_product($product){
        $articul = $this->get_product_articul($product);
        $count = $product['count'];
        return mysqli_query($this->link,"UPDATE test SET COUNT=COUNT+$count WHERE ARTICUL= '" . $articul . "'");
    }

    /**
     * @param $product
     */
    public function add_product($product){
        $articul = $product['articul'];
        $price = $product['price'];
        mysqli_query($this->link, "INSERT INTO test (ARTICUL, PRICE, COUNT) VALUES ('" . $articul . "', $price, 1)");
    }

    /**
     * @return mixed
     */
    public function total_product(){
        $query = "SELECT count(*) as total_res FROM test";
        $query_result = mysqli_query($this->link, $query);
        $row = $query_result->fetch_array(MYSQLI_ASSOC);
        return $row['total_res'];

    }

    /**
     * @return mixed
     */
    public function total_amount_product(){
        $query = "SELECT SUM(COUNT) as total_res FROM test";
        $query_result = mysqli_query($this->link, $query);
        $row = $query_result->fetch_array(MYSQLI_ASSOC);
        return $row['total_res'];
    }

    public function flush_products(){
        return mysqli_query($this->link,"UPDATE test SET COUNT=0 WHERE 1=1");
    }
}