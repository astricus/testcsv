<?php

require_once("config.php");
require_once("CsvParser.php");
require_once("Model.php");
set_time_limit(600);

$errors = "";
$result = "";
$Model = new Model($host, $user, $password, $db);
$total_product = $Model->total_product();
$total_amount_product = $Model->total_amount_product();

if (isset($_REQUEST['action']) && $_REQUEST['action'] == "exec_file") {
    $file = $uploaddir . basename($_FILES['file']['name']);
    $pathinfo = pathinfo($file);
    if($pathinfo['extension']!=="csv"){
        die("invalid file format (wait csv file only)");
    }
    if (move_uploaded_file($_FILES['file']['tmp_name'], $file)) {

        $CsvParser = new CsvParser;
        $parsed_file = $CsvParser->parse(file_get_contents($file));

        // сброс количества товаров на ноль у всех товаров, а затем увеличение количества в соответствии с импортируемым файлом
        $Model->flush_products();

        foreach ($parsed_file as $data_item){
            if($Model->check_product($data_item)==0){
                $Model->add_product($data_item);
            } else {
                $Model->update_product($data_item);
            }
        }

        $result = "<span class='success'>Файл успешно загружен</span>";
    } else {
        $errors.= "<span class='error'>Не удалось загрузить файл</span>";
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>парсер CSV - тестовое задание</title>
    <link href="style.css" rel="stylesheet" type="text/css"/>
</head>
<body>

<div class="form">
    <h1>Форма загрузки</h1>
    <span class="result"><?=$result?> <?=$errors?></span>
    <form name="parser_csv" method="post" enctype="multipart/form-data">
        <input name="action" type="hidden" value="exec_file">
        <input type="file" name="file" value=""> Файл в формате csv
        <input type="submit" value="Загрузить">
    </form>

    <b>Всего товаров в базе данных - <?=$total_product?></b>
    <b>Всего товаров (товар*количество)- <?=$total_amount_product?></b>

</div>

</body>
</html>